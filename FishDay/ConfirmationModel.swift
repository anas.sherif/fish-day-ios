//
//  ConfirmationModel.swift
//  FishDay
//
//  Created by Anas Sherif on 3/12/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class ConfirmationModel : NSObject
{
    var url = "verifications"
    var apiManager: ApiManager?
    var confirmationProtocol: ConfirmationProtocol?
    
    override init() {
        apiManager = ApiManager()
    }
    
    public func verify(user: User){
        let userJson = Mapper().toJSON(user)
        let userBody = ["user" : userJson]
        
        Alamofire.request(Constant.BASE_URL + url, method: .post, parameters: userBody, encoding: JSONEncoding.default, headers: ApiManager.headers).responseJSON{
            response in
            print(response)
            
            if response.result.isSuccess {
                let jsonRes = response.value as! NSDictionary
                print (jsonRes)
                
                let status = jsonRes["status"] as! String
                
                
                switch status {
                case "success":
                    let userJson = jsonRes["data"]
                    let user = Mapper<User>().map(JSON: userJson as! [String : Any])
                    self.confirmationProtocol?.onConfirmationSuccess(user: user!)
                    break
                case "fail":
//                    let message = jsonRes["message"] as! String
                    let message = "Error while verifying account"
                    self.confirmationProtocol?.onConfirmationError(message: message )
                    break
                default:
                    break
                }
                
            }else{
                self.confirmationProtocol?.onConfirmationError(message: "Errorrr while verifying")
            }
            
            
        }
    }
}
