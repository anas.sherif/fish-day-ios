//
//  ConfirmationViewController.swift
//  FishDay
//
//  Created by Anas Sherif on 3/12/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

class ConfirmationViewController: UIViewController, ConfirmationProtocol {

    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var verificationCodeField: UITextField!
    
    @IBOutlet weak var confirmAccountButton: UIButton!
    @IBOutlet weak var confirmMessage: UILabel!
    @IBOutlet weak var confirmCodeTitle: UILabel!
    @IBOutlet weak var confirmAccountTitle: UILabel!
    @IBOutlet weak var fourthTF: UITextField!
    @IBOutlet weak var thirdTF: UITextField!
    @IBOutlet weak var secondTF: UITextField!
    @IBOutlet weak var firstTF: UITextField!

    
    var confirmationModel = ConfirmationModel()
    var user = User()
    var isLogin = true
    var code : String?
    var mobileNumber : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        messageLabel.text = "ConfirmationCodeSentMessage".localized
        confirmCodeTitle.text = "ConfirmationCode".localized
        confirmAccountTitle.text = "ConfirmAccount".localized
        confirmAccountButton.setTitle("ConfirmAccount".localized, for: .normal)
        
        self.confirmationModel.confirmationProtocol = self
//        verificationCodeField.text = code
//        messageLabel.text = "Code has been sent\nplease write it\nhere"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Main" {
            let mainNavigationController = segue.destination as! SWRevealViewController
            
        }
    }
    private func verify(){
        SVProgressHUD.show()
        user.code = "\(firstTF.text ?? "")\(secondTF.text ?? "")\(thirdTF.text ?? "")\(fourthTF.text ?? "")"
        user.mobileNumber = self.mobileNumber

        print (user.code)
        print (user.mobileNumber)
        confirmationModel.verify(user: user)
    }
    func onConfirmationSuccess(user: User) {
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
        
        if isLogin {
            UserDefUtil.saveUser(user: user)
            performSegue(withIdentifier: "Main", sender: self)
        }
        else
        {
            UserDefUtil.saveUser(user: user)
            self.navigationController?.popViewController(animated: true)
        }
    }
    func onConfirmationError(message: String) {
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
        print(message)
    }
    @IBAction func confirmAccount(_ sender: Any) {
        verify()
    }

}

extension ConfirmationViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.count <= 1 {
            if textField == firstTF
            {
                firstTF.text = string
                secondTF.becomeFirstResponder()
            }
            else if textField == secondTF
            {
                secondTF.text = string
                thirdTF.becomeFirstResponder()
            }
            else if textField == thirdTF
            {
                thirdTF.text = string
                fourthTF.becomeFirstResponder()
            }
            else
            {
                fourthTF.text = string
                fourthTF.resignFirstResponder()
            }
            return false
        }
        return false
    }
}
