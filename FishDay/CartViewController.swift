//
//  CartViewController.swift
//  FishDay
//
//  Created by Muhammad Kamal on 2/17/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import UIKit
import SVProgressHUD
import MOLH

class CartViewController: UIViewController, CartProtocol{

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var cartBarButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var currencyyy: UILabel!
    @IBOutlet weak var currencyy: UILabel!
    @IBOutlet weak var tax: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var taxTitle: UILabel!
    @IBOutlet weak var subTotalTitle: UILabel!
    @IBOutlet weak var currencyTitle: UILabel!
    @IBOutlet weak var orderNowButton: UIButton!
    @IBOutlet weak var totalCostTitle: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    var cartModel = CartModel()
    var order = Order()
    var orderTotalPrice = Double()
    var orderItems = [OrderItem]()
    
    let notificationButton = SSBadgeButton()
    
    func addCartBarButton() {
        notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        notificationButton.setImage(UIImage(named: "menu_cart")?.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.addTarget(self, action: #selector(self.openCart), for: .touchUpInside)
        notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: notificationButton)
        let order = UserDefUtil.getOrder()
        if let count = order.orderItems?.count
        {
            notificationButton.badge = "\(count)"
        }
    }
    
    func openCart() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CartNavigation")
        //        let navController =  UINavigationController(rootViewController: vc)
        self.revealViewController().frontViewController = vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Cart".localized
        totalCostTitle.text = "TotalCost".localized
        currencyTitle.text  = "SAR".localized
        currencyy.text  = "SAR".localized
        currencyyy.text  = "SAR".localized
        taxTitle.text = "Tax".localized
        subTotalTitle.text = "SubTotal".localized
        orderNowButton.setTitle("OrderNow".localized, for: .normal)
        cartModel.cartProtocol = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if revealViewController() != nil{
            menuButton.target = self.revealViewController()
            if MOLHLanguage.isArabic()
            {
                menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            }
            else
            {
                menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            }
        }
        getCart()
        cartBarButton.setBadge(text: "9")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getCart(){
        SVProgressHUD.show()
        cartModel.getCart()
    }

    func onGettingCartSuccess(order: Order) {
        self.order = order
        UserDefUtil.saveOrder(order: order)
        orderItems = order.orderItems!
        SVProgressHUD.dismiss()
//        self.orderTotalPrice = Double(order.subTotal!)!
        self.tableView.reloadData()
        self.orderTotalPrice = Double(order.subTotal!)!
        self.subTotal.text = order.subTotal
        self.tax.text = order.tax
        self.totalPrice.text = order.total
    }
    func onGettingCartError(message: String) {
        print(message)
        SVProgressHUD.dismiss()
    }
    
    func onPlaceOrderSuccess(order: Order) {
        SVProgressHUD.dismiss()
        goToCompleteOrder()
    }
    func onPlaceOrderError(message: String) {
        SVProgressHUD.dismiss()
    }
    func onDeleteOrderItemSuccess(message: String) {
        print (message)
        SVProgressHUD.dismiss()
    }
    func onDeleteOrderItemError(message: String) {
        print (message)
        SVProgressHUD.dismiss()
    }
    func onIncreaseQuantity(orderItem: OrderItem, index: Int) {
        let newQuantity = Double((orderItem.quantity)! + 1)
        let newTotalPrice: Double = newQuantity * Double((orderItem.unitPrice)!)!
        orderItem.quantity = Int(newQuantity)
        orderItem.totalPrice = "\(newTotalPrice)"
        print ("Increase")
        tableView.reloadData()
        updateOrderTotalPrice(orderItem: orderItem, increase: true)
    }
    func onDecreaseQuantity(orderItem: OrderItem, index: Int) {
        if orderItem.quantity == 1
        {
            return
        }
        let newQuantity = Double((orderItem.quantity)! - 1)
        let newTotalPrice: Double = newQuantity * Double((orderItem.unitPrice)!)!
        orderItem.quantity = Int(newQuantity)
        orderItem.totalPrice = "\(newTotalPrice)"
        print ("Decrease")
        tableView.reloadData()
        updateOrderTotalPrice(orderItem: orderItem, increase: false)
    }
    func onDeleteItem(orderItem: OrderItem, index: Int) {
        SVProgressHUD.show()
        orderItems.remove(at: index)
        cartModel.deleteOrderItem(orderItemId: orderItem.id!)
        tableView.reloadData()
        print ("Delete")
    }
    private func updateOrderTotalPrice(orderItem: OrderItem, increase: Bool) {
        let unitPrice: Double = Double(orderItem.unitPrice!)!
        if (increase){
            self.orderTotalPrice += unitPrice
        }else{
            self.orderTotalPrice -= unitPrice
        }
        self.subTotal.text = "\(orderTotalPrice)"
        self.tax.text = "\((orderTotalPrice * 5 / 100))"
        self.totalPrice.text = "\(orderTotalPrice + (orderTotalPrice * 5 / 100))"
    }
    @IBAction func orderNow(_ sender: UIButton) {
        if orderItems.count == 0
        {
            showAlert(withMessage: "Cart is empty".localized)
            return
        }
        else if orderTotalPrice < 100
        {
            showAlert(withMessage: "Order cost should be more than 100 SAR".localized)
            return

        }
        SVProgressHUD.show()
        let order = Order()
        order.orderItems = self.orderItems
        cartModel.placeOrder(orderId: self.order.id!, order: order)
    }
    func goToCompleteOrder() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let completeOrderViewController = storyBoard.instantiateViewController(withIdentifier: "CompleteOrderViewController") as! CompleteOrderViewController
        navigationController?.pushViewController(completeOrderViewController, animated: true)
    }
    
    func showAlert(withMessage message: String,andTitle title: String? = nil,shouldShowCancelButton : Bool? = false,withOkAction acceptAction: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertActionStyle.default, handler: {action in
            if acceptAction != nil {
                acceptAction!()
            }
            alert.dismiss(animated: true, completion: nil)
        }))
        
        if shouldShowCancelButton! {
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        }
        present(alert, animated: true, completion: nil)
    }
    

}

extension CartViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath)
            as! CartTableViewCell
        let orderItem = orderItems[indexPath.row]
        cell.orderItem = orderItem
        cell.cartProtocol = self
        cell.indexPath = indexPath
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("Clickkkk")
    }
}
