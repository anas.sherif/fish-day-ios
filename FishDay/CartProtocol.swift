//
//  CartProtocol.swift
//  FishDay
//
//  Created by Anas Sherif on 3/16/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import UIKit

protocol CartProtocol{
    
    func onGettingCartSuccess(order: Order)
    func onGettingCartError(message: String)

    func onPlaceOrderSuccess(order: Order)
    func onPlaceOrderError(message: String)
    
    func onDeleteOrderItemSuccess(message: String)
    func onDeleteOrderItemError(message: String)
    
    
    func onIncreaseQuantity(orderItem: OrderItem, index: Int);
    func onDecreaseQuantity(orderItem: OrderItem, index: Int);
    func onDeleteItem(orderItem: OrderItem, index: Int);
    
}
