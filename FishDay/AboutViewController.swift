//
//  AboutViewController.swift
//  FishDay
//
//  Created by Medhat Mohamed on 6/30/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import UIKit
import MOLH

class AboutViewController: UIViewController {
    
    @IBOutlet weak var about: UITextView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    let notificationButton = SSBadgeButton()
    
    func addCartBarButton() {
        notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        notificationButton.setImage(UIImage(named: "menu_cart")?.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.addTarget(self, action: #selector(self.openCart), for: .touchUpInside)
        notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: notificationButton)
        let order = UserDefUtil.getOrder()
        if let count = order.orderItems?.count
        {
            notificationButton.badge = "\(count)"
        }
    }
    
    func openCart() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CartNavigation")
        //        let navController =  UINavigationController(rootViewController: vc)
        self.revealViewController().frontViewController = vc
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        addCartBarButton()

        title = "About".localized
        about.text = "AboutDesc".localized
        if revealViewController() != nil{
            menuButton.target = self.revealViewController()
            if MOLHLanguage.isArabic()
            {
                menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            }
            else
            {
                menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            }
            
        }
    }
}
