//
//  CompleteOrderViewController.swift
//  FishDay
//
//  Created by Medhat Mohamed on 3/24/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import UIKit
import GooglePlacePicker


class CompleteOrderViewController: UIViewController, CompleteOrderProtocol {
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var getCurrentAddressLabel: UIButton!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var completeOrderButton: UIButton!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var provideLabel: UILabel!
    @IBOutlet weak var notesTextView: UITextView!
    
    var completeOrderModel = CompleteOrderModel()
    var userLocation: GMSPlace?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.completeOrderModel.completeOrderProtocol = self
        selectLocation()
        mobileTextField.text = UserDefUtil.getUser().mobileNumber
        
        notesTextView.text = "Notes".localized
        notesTextView.textColor = UIColor.lightGray
    }
    
    @IBAction func getCurrentAddressAction(_ sender: UIButton) {
        selectLocation()
    }
    
    @IBAction func completeOrderAction(_ sender: UIButton) {
        let order = Order()
        order.userFullName = nameTextField.text
        order.userPhoneNumber = mobileTextField.text
        order.address = addressTextField.text
        order.addressLat = userLocation?.coordinate.latitude
        order.addressLon = userLocation?.coordinate.longitude
        order.notes = notesTextView.text
        completeOrderModel.completeOrder(orderId: UserDefUtil.getOrder().id!, order: order)
    }
    
    func goToOrderStatus() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let orderStatusViewController = storyBoard.instantiateViewController(withIdentifier: "OrderStatusViewController") as! OrderStatusViewController
        orderStatusViewController.isFromMenu = false
        navigationController?.pushViewController(orderStatusViewController, animated: true)
    }

    
    func selectLocation() {
        view.endEditing(true)
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    func onCompleteOrderSuccess(order: Order) {
        UserDefUtil.saveOrder(order: order)
        goToOrderStatus()
    }
    
    func onCompleteOrderError(message: String) {
        print(message)
        
    }
    
    func goToConfirmation() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let confirmationViewController = storyBoard.instantiateViewController(withIdentifier: "ConfirmationViewController") as! ConfirmationViewController
        confirmationViewController.isLogin = false
        confirmationViewController.mobileNumber = mobileTextField.text
        navigationController?.pushViewController(confirmationViewController, animated: true)
    }
}

extension CompleteOrderViewController: GMSPlacePickerViewControllerDelegate{
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        //        let riyadhlocation = CLLocation(latitude: 24.71355, longitude: 46.707866)
        //        let customerLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        //50 km radius
        //        if riyadhlocation.distance(from: customerLocation).isLessThanOrEqualTo(100000) {
        if let text = place.formattedAddress {
            addressTextField.text = text
        }else{
            addressTextField.text = "\(place.coordinate.latitude), \(place.coordinate.longitude)"
        }
        self.userLocation = place
        viewController.dismiss(animated: true, completion: nil)
        //        }else {
        //            let alert = UIAlertController(title: "MAPLIMIT".localized, message: "MAPLIMITMESSAGE".localized, preferredStyle: .alert)
        //            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
        //            viewController.present(alert, animated: true, completion: nil)
        //        }
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension CompleteOrderViewController: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Notes".localized
            textView.textColor = UIColor.lightGray
        }
    }
}
