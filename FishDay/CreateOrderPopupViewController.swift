//
//  CreateOrderPopupViewController.swift
//  FishDay
//
//  Created by Medhat Mohamed on 3/24/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import UIKit
import DropDown
import SVProgressHUD

class CreateOrderPopupViewController: UIViewController, CreateOrderItemProtocol {
    
    @IBOutlet weak var methodOfSlicingTitle: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var typeOfQuantityTitle: UILabel!
    @IBOutlet weak var typeOfQuantityLabel: UILabel!
    @IBOutlet weak var methodOfSlicingLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    var quantity: Int?
    var order: Order?
    var product: Product?
    var orderItem = OrderItem()
    var methodOfSlicing = 0
    var delegate : HomeProtocol?
    
    var createOrderItemModel = CreateOrderItemModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        methodOfSlicingTitle.text = "MethodOfSlicing".localized
        typeOfQuantityTitle.text = "TypeOfQuantity".localized
        cancelButton.setTitle("Cancel".localized, for: .normal)
        addToCartButton.setTitle("AddToCart".localized, for: .normal)
        
        createOrderItemModel.createOrderItemProtocol = self
        self.order = UserDefUtil.getOrder()
        self.typeOfQuantityLabel.text = "Per Kilo".localized
        self.methodOfSlicingLabel.text = "With cleaning".localized
        
    }
    @IBAction func increaseAction(_ sender: UIButton) {
        quantityLabel.text = "\(Int(quantityLabel.text!)! + 1)"
    }
    
    @IBAction func decreaseAction(_ sender: UIButton) {
        if quantityLabel.text != "1"
        {
            quantityLabel.text = "\(Int(quantityLabel.text!)! - 1)"
        }
    }
    
    @IBAction func addToCartAction(_ sender: UIButton) {
        SVProgressHUD.show()

        self.orderItem.orderId = order?.id
        self.orderItem.productId = self.product?.id
        self.orderItem.quantity = Int(self.quantityLabel.text!)
        if self.typeOfQuantityLabel.text == "Per Kilo".localized {
            self.orderItem.quantityType = 0
        }
        else
        {
            self.orderItem.quantityType = 1
        }
        self.orderItem.cuttingWay = self.methodOfSlicing
        
        self.createOrderItemModel.createOrderItem(orderItem: orderItem)
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func typeOfQuantityAction(_ sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.dataSource = ["Per Kilo".localized, "Per Piece".localized]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.typeOfQuantityLabel.text = item
        }
        dropDown.show()
    }
    
    @IBAction func methodOfSlicingAction(_ sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.dataSource = ["With cleaning".localized, "Without cleaning".localized, "For grilling".localized]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.methodOfSlicingLabel.text = item
            self.methodOfSlicing = index
        }
        dropDown.show()
    }
    
    func onCreatingOrderItemSuccess(order: Order) {
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
//        showAlert(withMessage: "Item Added To Cart")
        // show cart
        self.dismiss(animated: true, completion: nil)
        self.delegate?.openCart()
    }
    
    func onCreatingOrderItemError(message: String) {
        if SVProgressHUD.isVisible() {
            SVProgressHUD.dismiss()
        }
        showAlert(withMessage: message)
        
    }
    
    func showAlert(withMessage message: String,andTitle title: String? = nil,shouldShowCancelButton : Bool? = false,withOkAction acceptAction: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {[unowned self] action in
            if acceptAction != nil {
                acceptAction!()
            }
            self.dismiss(animated: true, completion: nil)
        }))
        
        if shouldShowCancelButton! {
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        }
        present(alert, animated: true, completion: nil)
    }
    
}
