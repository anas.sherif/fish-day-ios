//
//  Constant.swift
//  FishDay
//
//  Created by Anas Sherif on 3/4/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import UIKit

class Constant: NSObject {
    
//    static let BASE_URL = "https://samak-el-youm.herokuapp.com/api/v1/"
    static let BASE_URL = "http://www.fishday.com.sa/api/v1/"

    
    static let URL = "https://samak-el-youm.herokuapp.com/api/v1/home"
    
    static let API_KEY = "key=1d7801c576b33db841d59216d8cf91d4"
    
    static let USER = "user"
    static let ORDER = "order"
}
