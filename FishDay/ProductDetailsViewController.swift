//
//  ProductDetailsViewController.swift
//  FishDay
//
//  Created by Anas Sherif on 3/16/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import UIKit
import PopupDialog
import ImageSlideshow

class ProductDetailsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imagePager: ImageSlideshow!
    
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var perPieceLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var perKiloLabel: UILabel!
    
    @IBOutlet weak var productImage: UIImageView!

    @IBOutlet weak var piecePriceLabel: UILabel!
    @IBOutlet weak var kiloPriceLabel: UILabel!
    var product: Product?
    var imagesArray = [AlamofireSource]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addToCartButton.setTitle("AddToCart".localized, for: .normal)
        perKiloLabel.text = "PerKilo".localized
        perPieceLabel.text = "PerPiece".localized
        nameLabel.text = product?.name
        descLabel.text = product?.desc
        kiloPriceLabel.text = product?.kiloPrice
        piecePriceLabel.text = product?.piecePrice
        loadImage()
        if let productImages = product?.images
        {
            if productImages.count > 0
            {
                for item  in productImages
                {
                    imagesArray.append(AlamofireSource(urlString: item.small!)!)
                }
            }
        }
        imagePager.setImageInputs(imagesArray)
        imagePager.contentScaleMode = .scaleToFill
//        imagePager.setImageInputs([
//            ImageSource(image: #imageLiteral(resourceName: "fish") ),
//            ImageSource(image: #imageLiteral(resourceName: <#T##String#>))
//            AlamofireSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080"),
//            KingfisherSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080"),
//            ParseSource(file: PFFile(name:"image.jpg", data:data))
//            ])

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        imagePager.addGestureRecognizer(gestureRecognizer)
    }
    
    func didTap() {
        imagePager.presentFullScreenController(from: self)
    }

    private func loadImage() {
        if ((product?.images?[0].small) != nil) {
            let downloadURL = URL(string: (product?.images?[0].small)!)
            if downloadURL != nil {
                self.productImage.af_setImage(withURL: downloadURL!)
            } else {
                self.productImage.image = #imageLiteral(resourceName: "fish")
            }
        }
    }
    
    @IBAction func addToCartAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateOrderPopupViewController") as! CreateOrderPopupViewController
        // Create thec  dialog
        vc.product = product
        let popup = PopupDialog(viewController: vc, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        // Present dialog
        present(popup, animated: true, completion: nil)

    }
    
    func goToFullScreenImage(imageUrl : String)  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let fullScreenImageViewController = storyBoard.instantiateViewController(withIdentifier: "FullScreenImageViewController") as! FullScreenImageViewController
        navigationController?.pushViewController(fullScreenImageViewController, animated: true)
    }
    
}
