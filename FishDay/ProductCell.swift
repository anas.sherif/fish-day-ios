//
//  ProductCell.swift
//  FishDay
//
//  Created by Anas Sherif on 2/21/18.
//  Copyright © 2018 Anas Sherif. All rights reserved.
//

import UIKit
import AlamofireImage
class ProductCell: UICollectionViewCell
{
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var orderNowButton: UIButton!
    
    
    var product: Product!{
        didSet{
            self.productNameLabel.text = product.name
            self.productPriceLabel.text = product.kiloPrice
            if product.images != nil && (product.images?.count)! > 0 {
                if ((product.images?[0].small) != nil) {
                    let downloadURL = URL(string: (product.images?[0].small)!)
                    if downloadURL != nil {
                        self.productImage.af_setImage(withURL: downloadURL!)
                    } else {
                        self.productImage.image = #imageLiteral(resourceName: "fish")
                    }
                }
            }
            
        }
    }
}
